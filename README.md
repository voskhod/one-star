# One star

A small CLI game where you guess the title of the best movies using the worst
reviews ! (I saw the concept on twitter but I don't remember the name of the
peoples, sorry :()

```
Special fx fans will love it, but I was bored by a lacking story
While most of the world seems head over heels for this film, I can't get over
the fact that I was bored to tears. The movie seemed really long, and while
special effects are nice, a few hours into this thing I was more interested in
looking at my watch.I thought the whole premise of our entire existence being
fake was a good one. But, once seeing the movie, I realized that the premise
isn't really expanded that much. It's just developed enough to hold together
the action scenes. Maybe I would have been more entranced with a better cast.
But Keanu Reaves and Lawrence Fishburne don't exactly amount to genius in
casting.Maybe there's some element of this movie I'm just not getting, but if
this movie was the only thing on TV, I don't think I would watch


1: Hachi: A Dog's Tale                      2: In the Name of the Father               
3: The Matrix                               4: Braveheart                              
1
Wrong, the awser was The Matrix
Score: 1
```

# Build
* `cargo build`

# Usage
* `./one-star`: starts a game.
* `./one-star --list`: shows all the movies available.
* `./one-star --parse-imdb`: downloads the reviews of the top 250 from IMDB.
* `./one-star --add TITLE`: search and add a new movie.
* `./one-star --leaderboard`: shows the leaderboard.
