// This file is part of one-star.
//
// one-star is free software: you can redistribute it and/or modify it under
// the terms of the gnu general public license as published by the free
// software foundation, either version 3 of the license, or (at your option)
// any later version.
//
// one-star is distributed in the hope that it will be useful, but without
// any warranty; without even the implied warranty of merchantability or fitness
// for a particular purpose. see the gnu general public license for more
// details.
//
// You should have received a copy of the gnu general public license along with
// one-star. If not, see <https://www.gnu.org/licenses/>.

use crate::review::Review;

use reqwest::Client;

use error_chain::error_chain;
use std::io;

use lazy_static::lazy_static;
use regex::Regex;

#[derive(Debug)]
pub struct Entry {
    pub title: String,
    id: String,
}

error_chain! {
    types {
        Error, ErrorKind, ResultExt, Result;
    }

    foreign_links {
        ReqError(reqwest::Error);
        IoError(std::io::Error);
    }

    errors {
        NoEntryError(search: String) {
            description("No entry found in IMDB")
            display("No entry found for '{}'!", search)
        }
    }

    skip_msg_variant
}

lazy_static! {
    static ref CLIENT: Client = Client::builder().build().unwrap();
}

pub async fn get_top_100() -> Result<Vec<Entry>> {
    static LINK_TO_TOP: &str = "https://www.imdb.com/chart/top/?ref_=nv_mv_250";

    let top_100_html = CLIENT
        .get(LINK_TO_TOP)
        .header("Accept-Language", "en")
        .send()
        .await?
        .text()
        .await?;

    let top = extract_entry(top_100_html.as_str());
    Ok(top)
}

fn extract_entry(content: &str) -> Vec<Entry> {
    lazy_static! {
        static ref ENTRY_REGEX: Regex =
            Regex::new(r#"<a href="/title.(.+)/\?pf_rd_m=.*\n.*height="\d*" alt="(.+)"/>"#)
                .unwrap();
    }

    ENTRY_REGEX
        .captures_iter(content)
        .map(|cap| Entry {
            title: cap[2].to_string(),
            id: cap[1].to_string(),
        })
        .collect()
}

pub async fn get_reviews(movie: &Entry) -> Result<Vec<Review>> {
    // Only one start reviews sorted from most uselless to helpfulness.
    let reviews_link = format!(
        "https://www.imdb.com/title/{}/reviews?sort=\
        helpfulnessScore&dir=asc&ratingFilter=1",
        movie.id
    );

    let reviews_html = reqwest::get(reviews_link).await?.text().await?;

    let res = extract_review(movie.title.as_str(), reviews_html.as_str());
    Ok(res)
}

fn extract_review(movie: &str, content: &str) -> Vec<Review> {
    lazy_static! {
        static ref REVIEW_REGEX: Regex = Regex::new(
            r#"class="title" > (.+)\n.*\n.*\n.*class="review-date">(.+)</span>\n.*\n.*\n.*text show-more__control">(.+)</div>"#
        ).unwrap();

        static ref CLEAN1_REGEX: Regex = Regex::new(
            r#"&#39;"#
        ).unwrap();

        static ref CLEAN2_REGEX: Regex = Regex::new(
            r#"<br/>"#
        ).unwrap();
    }

    let spoiler = Regex::new(&regex::escape(movie)).unwrap();
    let mask = "*".repeat(movie.len());

    REVIEW_REGEX
        .captures_iter(content)
        .map(|cap| {
            let raw_content = html_escape::decode_html_entities(&cap[3]);
            let clean_content = CLEAN1_REGEX.replace_all(&raw_content, "\'");
            let clean_content = CLEAN2_REGEX.replace_all(&clean_content, "");
            let spoiler_content = spoiler.replace_all(&clean_content, &mask);

            let title = html_escape::decode_html_entities(&cap[1]);
            let title = spoiler.replace_all(&title, &mask).to_string();

            Review {
                movie: movie.to_string(),
                title,
                date: cap[2].to_string(),
                score: 1,
                content: spoiler_content.into_owned(),
            }
        })
        .collect()
}

pub async fn serch_and_get(movie: &str) -> Result<Vec<Review>> {
    let search = format!(
        "https://www.imdb.com/find?q={}&s=tt&ref_=fn_al_tt_ex",
        movie.replace(' ', "+")
    );

    let list_html = CLIENT
        .get(search.as_str())
        .header("Accept-Language", "en")
        .send()
        .await?
        .text()
        .await?;

    lazy_static! {
        static ref TITLES_REGEX: Regex = Regex::new(
            r#"/title/(tt\d+)/\?ref_=fn_tt_tt_\d" >([^<]+)</a> (\([a-zA-Z0-9\(\) ]*\))"#
        )
        .unwrap();
    }

    let entries: Vec<(String, String, String)> = TITLES_REGEX
        .captures_iter(list_html.as_str())
        .map(|c| (c[1].to_string(), c[2].to_string(), c[3].to_string()))
        .collect();

    if entries.is_empty() {
        return Err(ErrorKind::NoEntryError(movie.to_string()).into());
    }

    println!("Found {} entries in IMDB", entries.len());

    for (i, entry) in entries.iter().enumerate() {
        let (_, title, year_and_format) = entry;
        println!("[{:2}] {} {}", i + 1, title, year_and_format);
    }

    println!("Wich one do you want ?");
    let i = get_user_input(entries.len() + 1);
    let (id, title, _) = &entries[i - 1];

    get_reviews(&Entry {
        id: id.clone(),
        title: title.clone(),
    })
    .await
}

pub fn get_user_input(max: usize) -> usize {
    loop {
        let mut input_text = String::new();
        io::stdin()
            .read_line(&mut input_text)
            .expect("failed to read from stdin");

        let trimmed = input_text.trim();
        match trimmed.parse::<usize>() {
            Ok(i) => {
                if i > 0 && i < max {
                    return i;
                } else {
                    println!("Not an option, retry:");
                }
            }
            _ => println!("Not an integer, retry:"),
        };
    }
}
