// This file is part of one-star.
//
// one-star is free software: you can redistribute it and/or modify it under
// the terms of the gnu general public license as published by the free
// software foundation, either version 3 of the license, or (at your option)
// any later version.
//
// one-star is distributed in the hope that it will be useful, but without
// any warranty; without even the implied warranty of merchantability or fitness
// for a particular purpose. see the gnu general public license for more
// details.
//
// You should have received a copy of the gnu general public license along with
// one-star. If not, see <https://www.gnu.org/licenses/>.

use error_chain::error_chain;
use std::fs;
use std::io;
use yaml_rust::Yaml;
use yaml_rust::{YamlEmitter, YamlLoader};

pub struct Leaderboard {
    list: Vec<(u8, String)>,
}

error_chain! {
    foreign_links {
        IoError(std::io::Error);
    }
}

impl Leaderboard {
    pub fn new() -> Leaderboard {
        Leaderboard { list: vec![] }
    }

    pub fn load(path: &str) -> Result<Leaderboard> {
        let contents = fs::read_to_string(path)?;

        let leaderboard = YamlLoader::load_from_str(&contents).unwrap();

        let leaderboard = leaderboard[0].as_hash().unwrap();

        let scores = leaderboard
            .get(&Yaml::from_str("scores"))
            .unwrap()
            .as_vec()
            .unwrap();

        let names = leaderboard
            .get(&Yaml::from_str("names"))
            .unwrap()
            .as_vec()
            .unwrap();

        let leaderboard = names
            .iter()
            .zip(scores.iter())
            .map(|(name, score)| {
                (
                    score.as_i64().unwrap() as u8,
                    name.as_str().unwrap().to_string(),
                )
            })
            .collect();

        Ok(Leaderboard { list: leaderboard })
    }

    pub fn save(&self, path: &str) {
        let scores = self
            .list
            .iter()
            .map(|(score, _)| Yaml::Integer(*score as i64))
            .collect();

        let names = self
            .list
            .iter()
            .map(|(_, name)| Yaml::String(name.clone()))
            .collect();

        let yaml = {
            let mut h = yaml_rust::yaml::Hash::new();
            h.insert(Yaml::from_str("names"), Yaml::Array(names));
            h.insert(Yaml::from_str("scores"), Yaml::Array(scores));
            Yaml::Hash(h)
        };

        let mut out_str = String::new();
        let mut emitter = YamlEmitter::new(&mut out_str);
        emitter.dump(&yaml).unwrap();

        fs::write(path, out_str).unwrap();
    }

    pub fn update(&mut self, new_score: u8) {
        let pos = {
            let mut pos = 0;
            for (score, _) in &self.list {
                if *score < new_score {
                    break;
                }
                pos += 1;
            }
            pos
        };

        if pos < 10 {
            println!("New highscore ! Please enter your name:");
            let mut name = String::new();
            io::stdin()
                .read_line(&mut name)
                .expect("failed to read from stdin");
            name.pop();

            self.list.insert(pos, (new_score, name));

            if self.list.len() > 10 {
                self.list.pop();
            }
        }
    }

    pub fn display(&self) {
        for (score, name) in &self.list {
            println!("{:15} {}", name, score);
        }
    }
}
