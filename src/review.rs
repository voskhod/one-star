// This file is part of one-star.
//
// one-star is free software: you can redistribute it and/or modify it under
// the terms of the gnu general public license as published by the free
// software foundation, either version 3 of the license, or (at your option)
// any later version.
//
// one-star is distributed in the hope that it will be useful, but without
// any warranty; without even the implied warranty of merchantability or fitness
// for a particular purpose. see the gnu general public license for more
// details.
//
// You should have received a copy of the gnu general public license along with
// one-star. If not, see <https://www.gnu.org/licenses/>.

use std::fs;
use yaml_rust::yaml::{Hash, Yaml};
use yaml_rust::YamlEmitter;
use yaml_rust::YamlLoader;

#[derive(Debug)]
pub struct Review {
    pub movie: String,
    pub title: String,
    pub date: String,
    pub score: u8,
    pub content: String,
}

fn fetch_string(dict: &Hash, key: &str) -> String {
    dict.get(&Yaml::from_str(key))
        .unwrap()
        .as_str()
        .unwrap()
        .to_string()
}

impl Review {
    pub fn new(movie: &str, raw_vals: &Hash) -> Review {
        let score = raw_vals
            .get(&Yaml::from_str("score"))
            .unwrap()
            .as_i64()
            .unwrap();

        Review {
            movie: movie.to_string(),
            title: fetch_string(raw_vals, "title"),
            date: fetch_string(raw_vals, "date"),
            score: score as u8,
            content: fetch_string(raw_vals, "content"),
        }
    }

    pub fn from_file(path: &str, movie: &str) -> Vec<Review> {
        let mut parsed_reviews = Vec::new();

        let contents = fs::read_to_string(path).expect("Something went wrong reading the file");

        let reviews = YamlLoader::load_from_str(&contents).expect("Not a correct yaml file");

        for r in reviews {
            let r = r
                .as_hash()
                .unwrap_or_else(|| panic!("failed to parse {} at {}", movie, path));
            parsed_reviews.push(Review::new(movie, r));
        }

        parsed_reviews
    }

    pub fn save(path: &str, reviews: &Vec<Review>) {
        let mut out_str = String::new();

        for r in reviews {
            let mut emitter = YamlEmitter::new(&mut out_str);
            emitter.dump(&r.to_yaml()).unwrap();
            out_str.push('\n');
        }

        fs::write(path, out_str).unwrap();
    }

    fn to_yaml(&self) -> Yaml {
        let mut yaml = Hash::new();
        yaml.insert(Yaml::from_str("title"), Yaml::String(self.title.clone()));
        yaml.insert(Yaml::from_str("date"), Yaml::String(self.date.clone()));
        yaml.insert(Yaml::from_str("score"), Yaml::Integer(self.score as i64));
        yaml.insert(
            Yaml::from_str("content"),
            Yaml::String(self.content.clone()),
        );

        Yaml::Hash(yaml)
    }
}
