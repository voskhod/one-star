// This file is part of one-star.
//
// one-star is free software: you can redistribute it and/or modify it under
// the terms of the gnu general public license as published by the free
// software foundation, either version 3 of the license, or (at your option)
// any later version.
//
// one-star is distributed in the hope that it will be useful, but without
// any warranty; without even the implied warranty of merchantability or fitness
// for a particular purpose. see the gnu general public license for more
// details.
//
// You should have received a copy of the gnu general public license along with
// one-star. If not, see <https://www.gnu.org/licenses/>.

use crate::review::Review;
use rand::Rng;
use std::io;

pub fn get_user_input() -> u8 {
    loop {
        let mut input_text = String::new();
        io::stdin()
            .read_line(&mut input_text)
            .expect("failed to read from stdin");

        let trimmed = input_text.trim();
        match trimmed.parse::<u8>() {
            Ok(i) => {
                if i > 0 && i < 5 {
                    return i;
                } else {
                    println!("Not an anwser, retry:");
                }
            }
            _ => println!("Not an integer, retry:"),
        };
    }
}

pub fn game(reviews: &Vec<Review>, movies: &Vec<String>) -> u8 {
    let mut score = 0;
    let mut rng = rand::thread_rng();
    let mut available_answers = Vec::with_capacity(4);

    for i in 0..10 {
        println!("Review {}/10:\n", i + 1);

        let idx = rng.gen_range(0..reviews.len());
        let r = &reviews[idx];
        println!("{}", r.title);
        print_content(&r.content);
        print!("\n\n");

        let pos_of_valid_movie = rng.gen_range(0..4);
        for pos in 0..4 {
            if pos == pos_of_valid_movie {
                available_answers.push(&r.movie);
            } else {
                let bad_answer = {
                    loop {
                        let i = rng.gen_range(0..movies.len());
                        let anwser = &movies[i];

                        if *anwser != r.movie && !available_answers.contains(&anwser) {
                            break anwser;
                        }
                    }
                };

                available_answers.push(bad_answer);
            }
        }

        println!(
            "1: {:40} 2: {:40}\n3: {:40} 4: {:40}",
            available_answers[0], available_answers[1], available_answers[2], available_answers[3]
        );

        available_answers.clear();

        if get_user_input() - 1 == pos_of_valid_movie {
            score += 1;
            println!("Correct !\nScore: {}\n\n", score);
        } else {
            println!("Wrong, the awser was {}\nScore: {}\n\n", r.movie, score);
        }
    }

    println!("Congratulation, your score is {}.", score);
    score
}

fn print_content(content: &String) {
    let len = content.len();
    let content = content.as_str();

    let mut begin = 0;
    while begin < len {
        let mut end = begin + 80;

        if end < len {
            loop {
                if &content[end - 1..end] == " " {
                    break;
                }
                end -= 1;
            }
        } else {
            end = len;
        }

        println!("{}", &content[begin..end - 1]);
        begin = end;
    }
}
