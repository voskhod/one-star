// This file is part of one-star.
//
// one-star is free software: you can redistribute it and/or modify it under
// the terms of the gnu general public license as published by the free
// software foundation, either version 3 of the license, or (at your option)
// any later version.
//
// one-star is distributed in the hope that it will be useful, but without
// any warranty; without even the implied warranty of merchantability or fitness
// for a particular purpose. see the gnu general public license for more
// details.
//
// You should have received a copy of the gnu general public license along with
// one-star. If not, see <https://www.gnu.org/licenses/>.

mod game;
mod imdb;
mod leaderboard;
mod review;

use crate::leaderboard::Leaderboard;
use crate::review::Review;

use clap::{App, Arg};

use error_chain::error_chain;
use std::fs;

error_chain! {
    types {
        Error, ErrorKind, ResultExt, Result;
    }

    foreign_links {
        ReqError(reqwest::Error);
        IoError(std::io::Error);
    }

    errors {
        NotEnoughMovies(path: String) {
            description("Not enough movies")
            display("Not enough movies in '{}'!", path)
        }
    }

    skip_msg_variant
}

fn play() -> Result<()> {
    let (reviews, movies) = {
        let paths = fs::read_dir("reviews/")?;

        let mut reviews = vec![];
        let mut movies = vec![];

        for p in paths {
            let p = p.unwrap();

            let path = p.path().into_os_string().into_string().unwrap();

            let movie = p.file_name().into_string().unwrap();
            let movie = &movie[0..movie.len() - 5];

            let mut r = Review::from_file(path.as_str(), movie);

            reviews.append(&mut r);
            movies.push(movie.to_string());
        }

        (reviews, movies)
    };

    if reviews.len() < 4 {
        return Err(ErrorKind::NotEnoughMovies("reviews/".to_string()).into());
    }

    let mut leaderboard = match Leaderboard::load("leaderboard.yaml") {
        Ok(l) => l,
        Err(_) => Leaderboard::new(),
    };

    loop {
        let score = game::game(&reviews, &movies);
        leaderboard.update(score);
        leaderboard.display();

        println!("Enter 1 to retry and 2 to quit");
        if game::get_user_input() != 1 {
            break;
        }
    }
    leaderboard.save("leaderboard.yaml");

    Ok(())
}

#[tokio::main]
async fn generate_reviews() {
    let top = imdb::get_top_100().await.unwrap();
    let mut prog = progress::Bar::new();

    let mut failed: Vec<&str> = Vec::with_capacity(250);

    // Create 'reviews/' if it does not exist yet.
    if fs::metadata("reviews").is_err() {
        fs::create_dir("reviews").unwrap();
    }

    for (cur, movie) in top.iter().enumerate() {
        let reviews = match imdb::get_reviews(movie).await {
            Ok(reviews) => reviews,
            Err(msg) => {
                println!("{} has failed: {}\n", movie.title, msg);
                failed.push(movie.title.as_str());
                continue;
            }
        };

        let file = format!("reviews/{}.yaml", movie.title);
        Review::save(&file, &reviews);

        prog.reach_percent((cur * 100 / 250) as i32);
        print!("\r{}", movie.title);
    }

    prog.jobs_done();

    if !failed.is_empty() {
        println!("Those movies has failed:");

        for f in failed {
            println!("\t{}", f);
        }
    }
}

fn display_leaderboard() {
    match Leaderboard::load("leaderboard.yaml") {
        Ok(l) => l.display(),
        Err(_) => println!("No leaderboard yet !"),
    };
}

fn list() {
    let paths = fs::read_dir("reviews/").unwrap();

    for p in paths {
        let p = p.unwrap();

        let movie = p.file_name().into_string().unwrap();

        println!("{}", &movie.as_str()[0..movie.len() - 5]);
    }
}

#[tokio::main]
async fn add_movie(movie: &str) {
    let reviews = match imdb::serch_and_get(movie).await {
        Ok(r) => r,
        Err(msg) => {
            println!("{}", msg);
            return;
        }
    };

    if reviews.is_empty() {
        println!("No one-star review :(");
        return;
    }

    // Create 'reviews/' if it does not exist yet.
    if fs::metadata("reviews").is_err() {
        fs::create_dir("reviews").unwrap();
    }

    let path = format!("reviews/{}.yaml", reviews[0].movie);
    Review::save(path.as_str(), &reviews);
    println!("Done ! Saved under {}.", path);
}

fn main() {
    let matches = App::new("\u{2B50}One-star \u{2B50}")
        .version("1.0.0")
        .author("Jérôme Dubois")
        .about("\u{1F980}")
        .arg(
            Arg::with_name("leaderboard")
                .long("leaderboard")
                .short("l")
                .help("Show the leaderboard"),
        )
        .arg(
            Arg::with_name("parse-imdb")
                .long("parse-imdb")
                .short("p")
                .help("Fetch and parse reviews from IMDB from the 250 top movies"),
        )
        .arg(
            Arg::with_name("list")
                .long("list")
                .help("Show all the movies available"),
        )
        .arg(
            Arg::with_name("add")
                .long("add")
                .short("a")
                .help("Search and add a new movie")
                .value_name("TITLE")
                .takes_value(true),
        )
        .get_matches();

    if matches.is_present("leaderboard") {
        display_leaderboard();
    } else if matches.is_present("parse-imdb") {
        generate_reviews();
    } else if matches.is_present("list") {
        list();
    } else if matches.is_present("add") {
        add_movie(matches.value_of("add").unwrap());
    } else {
        match play() {
            Ok(_) => {}
            Err(_) => {
                println!(
                    "Cannot play with less than 4 movies, try to \
                generates the reviews using one-star --parse-imdb first."
                );
            }
        }
    }
}
